import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Cliente } from '../models/cliente';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private baseUrl = 'http://localhost:9899/cliente/';
  constructor(private http: HttpClient) { }

//metodo para guardar al cliente  +++++++++++++++++++
  createCliente(cliente: Cliente) {
    const headers = { "content-type": "application/json" };
    let body = {
      "id_cliente": cliente.id_cliente.toString(),
      "cedula": cliente.cedula.toString(),
      "nombre": cliente.nombre.toString(),
      "eliminado":cliente.eliminado.valueOf(),
      "telefono": cliente.telefono.toString(),
      "direccion": cliente.direccion.toString(),
      "fecha_creacion": cliente.fecha_creacion.toString(),
      "fecha_modificacion": cliente.fecha_modificacion.toString()
    }
    return this.http.post(this.baseUrl + "guardar", body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

 

  //metodo para listar los datos existentes +++++++++++++++++++++++
  listarCliente(): Observable<[Cliente]> {
    return this.http.get<[Cliente]>(`${this.baseUrl+'listatrue'}`);
  }

 //metodo para modificar +++++++++++++++++++++++++
  editarCliente(id: string, cliente: Cliente) {
    const headers = { "content-type": "application/json" };
    let body = {
      "id_cliente": cliente.id_cliente.toString(),
      "cedula": cliente.cedula.toString(),
      "nombre": cliente.nombre.toString(),
      "eliminado":cliente.eliminado.valueOf(),
      "telefono": cliente.telefono.toString(),
      "direccion": cliente.direccion.toString(),
      "fecha_creacion": cliente.fecha_creacion.toString(),
      "fecha_modificacion": cliente.fecha_modificacion.toString()
    }
    return this.http.put(this.baseUrl + "editar/"+id, body, {
      headers: headers, observe: "response"
    })
      .toPromise();
  }

    //buscar los datos del cliente por su cedula
  buscarPorCedula(cedula: string): Observable<Cliente> {
    return this.http.get<Cliente>(this.baseUrl + '/buscarporcedula/' + cedula);
  }

  //Eliminar cliente
  eliminarCliente(cedula :string, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl + 'eliminar'}/${cedula}`, value);
  }
  
}
