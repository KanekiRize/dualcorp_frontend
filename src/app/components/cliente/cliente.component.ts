import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from 'src/app/models/cliente';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';
import swal from'sweetalert2';

import { ClienteService } from 'src/app/services/cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
   alerta:string='';
   id_clientes: any;
   cliente: Cliente[];
   id_cliente: number;
   cedula: string;
   nombre:string;
   direccion: string;
   eliminado: boolean;
   telefono: any
   fecha: string = new Date().toISOString();
   fecha_creacion:Date
   fecha_modificacion: Date
   clientes:Cliente
   page:number = 1;
  page1:number = 1;
  page2:number = 1;

  constructor(private clienteServivio: ClienteService, private  router: Router ) {
    
  }
  ngOnInit() {
    this.listar();
  }
  resfrecra(){
    window.location.reload();
  }
  limpiar(){
    this.cedula= '';
  this.nombre= '';
  this.direccion= '';
  this.telefono= '';
  }
 //metodo para listar a todos los clientes++++++++++++++++++++++++++++
 listar() {
  this.clienteServivio.listarCliente().subscribe ((data: any)=>{
    this.cliente= data
    
  })

}

 //metodo para registrar++++++++++++++++++++++++++++++++++++++++
 GurdarlosUsuario() {
  let dataCliente: Cliente;
  
   dataCliente = {
     id_cliente: 8000,
    cedula: this.cedula,
     nombre: this.nombre,
    direccion: this.direccion,
    eliminado: true,
    telefono: this.telefono.toString(),
    fecha_creacion: this.fecha.toString(),
   fecha_modificacion: this.fecha.toString(),

 
  };
  swal.fire({
    position: 'center',
    icon: 'warning',
    title: 'guardado',
    showConfirmButton: false,
    timer: 1500
  });
 this.clienteServivio.createCliente(dataCliente).then((data)=>{
  this.ngOnInit()
  console.log("holaaaa"+data)

   this.cedula= '';
  this.nombre= '';
  this.direccion= '';
  this.telefono= '';

 })
 
 
} 
//metodo para buscar por id ++++++++++++++++++++++++++++++++
obtenercliente(cedula: string) {
  this.clienteServivio.buscarPorCedula(cedula).subscribe((data) => {
   // console.log(data.cedula )
    this.clientes= data
    this.id_cliente = data.id_cliente;
    this.cedula = data.cedula;
     this.nombre = data.nombre;
     this.direccion = data.direccion;
     this.telefono = data.telefono;
     this.fecha_creacion = data.fecha_creacion;
     this.fecha_modificacion = data.fecha_modificacion;
  });
  
}
//metod para modificar **********************************
modificar(){
  let dataCliente: Cliente
  dataCliente = {
    id_cliente: this.id_cliente,
   cedula: this.cedula,
    nombre: this.nombre,
   direccion: this.direccion,
   eliminado: true,
   telefono: this.telefono.toString(),
   fecha_creacion: this.fecha_creacion,
  fecha_modificacion: this.fecha.toString(),
 
 };
 
 this.clienteServivio.editarCliente(this.cedula,dataCliente);
 console.log("holaaaasss")
 this.ngOnInit();
 
 swal.fire({
  position: 'center',
  icon: 'success',
  title: 'Registro Actualizado',
  showConfirmButton: false,
  timer: 1500
});
//window.location.reload();
}

//metodo para elimminar al cliete por medio de estado++++++++++
eliminar2(cedula: string){
if(cedula!=''){
  swal.fire({
    title: 'Estas seguro?',
    text: "Ya no se podra recuperar el registro!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Eliminar!'
  }).then((result) => {
    if (result.isConfirmed) {
      swal.fire(
        'Eliminado',
        'Registro ha sigo eliminado.',
        'success'
      )
      this.clienteServivio.eliminarCliente(cedula,this.cliente).subscribe((data) => {
        console.log("eliminado " + data)
        console.log("eliminado" + cedula)
        this.ngOnInit()
      
       });
       
    }
  })
  
}
 }
}



